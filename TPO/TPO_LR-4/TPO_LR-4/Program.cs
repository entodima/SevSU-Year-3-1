﻿using System;
using System.IO;

namespace TPO_LR_4
{
    class Program
    {

        static void Main(string[] args)
        {
            Tester tester = new Tester();
            tester.runTest();
        }
    }

    class Tester
    {
        static private StreamWriter log = new StreamWriter("log.log");
        static public void Log(string msg)
        {
            log.WriteLine(msg);
        }
        static public void Close()
        {
            log.Close();
        }

        public void runTest()
        {
            NmbReaderTest();
            Close();
        }
        public void NmbReaderTest()
        {
            NmbWriter nmbrs = new NmbWriter();
            nmbrs.nmb1 = 1000;
            nmbrs.nmb2 = 100;

            nmbrs.save("out.txt");
            NmbReader nmbReader = new NmbReader();
            NmbWriter readNmrsFromFile = new NmbWriter();

            readNmrsFromFile = nmbReader.readNmbrs("out.txt");

            if (nmbrs.nmb1 == readNmrsFromFile.nmb1 && nmbrs.nmb2 == readNmrsFromFile.nmb2) {
                Console.WriteLine("Тест интеграции пройден!");
                Log("Тест интеграции пройден!");
            } else {
                Console.WriteLine("Тест интеграции провален!");
                Log("Тест интеграции провален!");
            }
        }
    }

    class NmbReader
    {
        public NmbWriter readNmbrs(string filename)
        {
            NmbWriter calc = new NmbWriter();
            string context = File.ReadAllText(filename);
            string[] strs;
            strs = context.Split();
            calc.nmb1 = Int32.Parse(strs[0]);
            calc.nmb2 = Int32.Parse(strs[1]);
            return calc;
        }
    }

    class NmbWriter
    {
        public int nmb1;
        public int nmb2;

        public void save(string filename)
        {
            File.WriteAllText(filename, nmb1 + " " + nmb2);
        }
    }
}
