﻿using System;
using System.IO;

namespace TPO_LR_1 {
    class Program {
        static void Main(string[] args) {
            Console.WriteLine("----- Задание 1 ------");
            Console.Write("Введите размерность матрицы: ");
            int size = Int32.Parse(Console.ReadLine());
            int[,] arr = new int[size, size];

            Random rnd = new Random();
            //заполенение массива случайными значениями
            for (int i = 0; i < size; i++) for (int j = 0; j < size; j++)
                    arr[i, j] = rnd.Next(1, 10);
            PrintArray(arr);

            Console.Write("Введите количество поворотов: ");
            int n = Int32.Parse(Console.ReadLine());
            if (n > 0) n = n % 4;
            else n = n % -4;

            arr = rotateMatrix(arr, n);
            PrintArray(arr);

            
            Console.WriteLine("----- Задание 2 ------");
            Console.Write("Введите строку: ");
            if (EInString(Console.ReadLine())) Console.WriteLine("В строке есть повторяющаяся 5 раз буква \"e\"");
            else Console.WriteLine("В строке нет повторяющейся 5 раз буквы \"e\"");
            

            
            Console.WriteLine("----- Задание 3 ------");
            string path = @"C:\Users\eatmo\Desktop\Text.txt";
            
            try {
                int count = 0;
                using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default)) {
                    string line;
                    while ((line = sr.ReadLine()) != null) {
                        if (line == "") count++;
                    }
                    Console.WriteLine(count);
                }
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
            
            Console.ReadKey();
        }

        static void PrintArray(int[,] arr) {
            for (int i = 0; i < arr.GetLength(0); i++) {
                for (int j = 0; j < arr.GetLength(0); j++) Console.Write(arr[i, j] + "\t");
                Console.WriteLine();
            }
        }

        static int[,] rotateMatrix(int[,] arr, int n) {
            int[,] temp_arr = new int[arr.GetLength(0), arr.GetLength(0)];
            if (n == -3) n = 1;
            if (n == -2) n = 2;
            if (n == -1) n = 3;

            for (int k = 0; k < n; k++) {
                for (int i = 0; i < arr.GetLength(0); i++) {
                    for (int j = 0; j < arr.GetLength(0); j++) {
                        temp_arr[i, j] = arr[j, arr.GetLength(0) - i - 1];
                    }
                }
            }

            return temp_arr;
        }

        static bool EInString(string str) {
            int n = 0;
            for (int i = 0; i < str.Length; i++) {
                if (str[i] == 'e' || str[i] == 'е') {
                    n++;
                    if (n == 5) return true;
                } else n = 0;
            }
            return false;
        }
    }
}
