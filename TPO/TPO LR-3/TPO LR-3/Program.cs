﻿using System;
using System.IO;

namespace TPO_LR_1 {
    class Program {

        static void Main(string[] args) {
            EStringTester eStringTester = new EStringTester();
            Log.Close();
            
            Console.WriteLine("Результат выведен в Log");
            Console.ReadKey();
        }
    }

    public class EInString {
        public string str;
        
        public bool CheckString() {
            
            int n = 0;
            for (int i = 0; i < str.Length; i++) {
                if (str[i] == 'e' || str[i] == 'е') {
                    n++;
                    if (n == 5) return true;
                } else n = 0;
            }
            return false;
        }
    }

    public class Log {
        static private StreamWriter logfile = new StreamWriter("log.log");
        static public void AddString(string message) {
            logfile.WriteLine(message);
        }

        static public void Close() {
            logfile.Close();
        }
    }

    abstract class Tester {
        protected void LogMessage(string message) {
            Log.AddString(message);
        }
    }

    class EStringTester : Tester {
        EInString OUT;

        public EStringTester() {
            OUT = new EInString();
            Run();
        }
        private void Run() {
            StringTest1();
        }
        private void StringTest1() {
            string[] strings = {
                "e",
                "eeeeeОдиндва три четыре",
                "Одиндва триeeeee четыре",
                "Одиндва три четeeeeeыре",
                "eeeee"};
            
            foreach (String str in strings) {
                
                OUT.str = str;
                if (OUT.CheckString()) Log.AddString("е повторяется\t:\t"+str);
                else Log.AddString("е не повторяется\t:\t" + str);
            }
        }
    }
}