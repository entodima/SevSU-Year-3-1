using System;
using Xunit;
using ConsoleApp1;

namespace XUnitTestProject1
{
    public class UnitTest1
    {
        string[] strings = {
                "e",
                "eeeee������� ��� ������",
                "������� ���eeeee ������",
                "������� ��� ���eeeee���",
                "eeeee"};
        bool[] results = {
                false,
                true,
                true,
                true,
                true};

        [Fact]
        public void EInStrings_Check()
        {
            for (int i = 0; i < strings.Length; i++)
            {
                string str = strings[i];
                EInString estr = new EInString(str);
                estr.CheckAndPrint();
                Assert.Equal(estr.result, results[i]);
            }
        }

        [Fact]
        public void EInStrings_E()
        {
            for (int i = 0; i < strings.Length; i++)
            {
                string str = strings[i];
                EInString estr = new EInString(str);
                Assert.Equal(estr.EIn(str), results[i]);
            }
        }
    }
}
