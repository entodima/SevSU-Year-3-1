import UIKit

class EInString {
    public var str = String()
    
    func CheckString() -> Bool {
        for char in str {
            if(char=="e" || char=="е") {
                return true
            }
        }
        return false
    }
}

class EStringTester {
    var OUT = EInString()
    
    init() {
        Run()
    }
    
    private func Run() {
        StringTest1()
    }
    
    private func StringTest1() {
        let strings = ["e",
                       "eeeeeОдиндва три четыре",
                       "Одиндва триeeeee четыре",
                       "Одиндва три четeeeeeыре",
                       "eeeee",
                       "lsjlkjoiu483"]
        let results = [true, true, true, true, true, false]
        
        for i in 0...strings.count-1 {
            OUT.str = strings[i]
            if(OUT.CheckString() == results[i]) {
                print("тест пройден: "+strings[i])
            } else {print("тест провален: "+strings[i])}
        }
    }
}

var eStringTester = EStringTester()


