﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using TPO_LR_1;

namespace TPO_LR_1
{
    public class EClassTests
    {
        

        [Fact]
        public void CheckAndPrint()
        {
            for (int i = 0; i < strings.Length; i++)
            {
                string str = strings[i];
                EInString estr = new EInString(str);
                estr.CheckAndPrint();
                Assert.Equal(estr.result, results[i]);
            }
            
        }

        [Fact]
        public bool EInString()
        {
            return true;
        }
    }
}
