﻿using System;
using System.Collections.Generic;

namespace LR_1 {
    class Program {
        static void Main(string[] args) {
            int[,] array = {
                {0,1,1,1,1,0,0,0},
                {0,0,0,0,0,0,0,0},
                {1,1,0,0,1,1,0,0},
                {0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,1},
                {0,0,0,1,1,0,0,0},
                {0,0,0,0,1,1,0,0}
            };
            List<List<int>> floors = new List<List<int>>();
            List<int> used = new List<int>();

            while(used.Count < array.GetLength(0)) {
                var temp = ZeroCollumns(array, used);
                floors.Add(temp);
                foreach (var element in temp) if (!used.Contains(element)) used.Add(element);

                OutArray(array, used);
                OutList(floors);
            }
            Console.ReadKey();
        }

        static List<int> ZeroCollumns(int[,] array, List<int> used) {
            bool[] isCollumnZero = new bool[array.GetLength(0)];

            for (int i = 0; i < isCollumnZero.GetLength(0); i++) isCollumnZero[i] = true;
            for (int i = 0; i < used.Count; i++) isCollumnZero[used[i]] = false;

            for (int i = 0; i < array.GetLength(0); i++) for (int j = 0; j < array.GetLength(0); j++) 
                if (array[i, j] == 1 && !used.Contains(i)) isCollumnZero[j] = false;
                
            List<int> floor = new List<int>();
            for (int i = 0; i < array.GetLength(0); i++) if (isCollumnZero[i]) floor.Add(i);
            OutList(floor);
            return floor;
        }


        static void OutArray(int[,] array, List<int> used) {
            for (int i = 0; i < array.GetLength(0); i++) {
                for (int j = 0; j < array.GetLength(0); j++)
                    if (used.Contains(i) || used.Contains(j)) Console.Write("-"+"   ");
                    else Console.Write(array[i, j] + "   ");
                Console.WriteLine();
            }
            Console.WriteLine("------------------------------");
        }

        static void OutList(List<int> list) {
            foreach (int element in list)
                Console.Write(element+1 + "   ");
            Console.WriteLine();
            Console.WriteLine("------------------------------");
        }

        static void OutList(List<List<int>> lists) {
            foreach (List<int> list in lists) {
                foreach (int element in list) Console.Write(element+1 + "   ");
                Console.WriteLine();
            }
            Console.WriteLine("------------------------------");
        }
    }
}
