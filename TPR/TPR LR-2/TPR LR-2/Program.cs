﻿using System;
using System.Collections.Generic;

namespace TPR_LR_2 {
    class Program {
        static void Main(string[] args) {
            int[,] a1 = {
                { 0, 0, 0, 0, 0, 0, 0 },
                { 1, 0, 1, 0, 0, 1, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 1, 0, 0, 1, 0, 0 },
                { 1, 0, 1, 0, 0, 1, 0 },
                { 0, 0, 0, 0, 0, 0, 0 },
                { 0, 1, 0, 0, 1, 0, 0 }
            };
            int[,] a2 = {
                { 1, 0, 1, 0, 0, 1, 0 },
                { 0, 1, 0, 0, 1, 0, 0 },
                { 1, 0, 1, 0, 0, 1, 0 },
                { 0, 0, 0, 1, 0, 0, 1 },
                { 0, 1, 0, 0, 1, 0, 0 },
                { 1, 0, 1, 0, 0, 1, 0 },
                { 0, 0, 0, 1, 0, 0, 1 }
            };

            List<List<int>> r = new List<List<int>>();

            for (int i = 0; i < MathF.Sqrt(a2.Length); i++) {
                var temp = new List<int>();
                for (int j = 0; j < MathF.Sqrt(a2.Length); j++) if (a2[i, j] == 1) temp.Add(j + 1);
                r.Add(temp);
            }

            List<List<int>> k = new List<List<int>>();
            for(int i=0;i< (r.Count); i++) {
                var temp = new List<int>();
                for (int j=0; j<(r[i].Count); j++) {
                    temp.Add(r[i][j]);
                }
                k.Add(temp);
            }

            for (int i = 0; i < k.Count; i++) {
                for (int j = i+1; j < k.Count; j++) {
                    if (IsListsEquals(k[i], k[j])) k.RemoveAt(j);
                }
            }

            //создание массива U(k)
            List<int> U_k = new List<int>();
            for (int i = 0; i < k.Count; i++)
            {
                if (i == 0) U_k.Add(0);
                else
                {
                    var temp = false;
                    for(int j=0;j<k[i].Count; j++)
                    {
                        for(int l=0; l<i; l++)
                        {
                            for (int m = 0; m < k[l].Count; m++)
                            {

                                if (a1[k[i][j]-1, k[l][m]]-1 == 1)
                                {
                                    temp = true;
                                }

                            }
                        }
                    }
                    if (temp == false)
                    {
                        U_k.Add(i+1);
                    }
                    else
                    {
                        U_k.Add((i+1)*-1);
                    }
                }
            }


            Console.WriteLine("R:");
            PrintList(r);
            Console.WriteLine("K:");
            PrintList(k);
            Console.WriteLine("U(K):");
            PrintList(U_k);

            Console.WriteLine("U(x):");
            for (int i=k.Count-1; i >=0; i--) {
                for(int j=0; j<k[i].Count; j++) {
                    Console.WriteLine("x"+k[i][j]+" = " + U_k[i]);
                }
            }

            Console.ReadKey();
        }

        static void PrintList(List<List<int>> listOfLists) {
            foreach (List<int> row in listOfLists) {
                foreach (int coll in row) {
                    Console.Write(coll);
                }
                Console.WriteLine();
            }
            Console.WriteLine("------");
        }

        static void PrintList(List<int> list) {
            for(int i=0; i<list.Count; i++) {
                Console.WriteLine("U("+(i+1)+") = "+list[i]);
            }
            Console.WriteLine("------");
        }


        static bool IsListsEquals(List<int> r1, List<int> r2) {
            if (r1.Count.Equals(r2.Count)) {
                for(int i=0; i<r2.Count; i++) {
                    if (!r1[i].Equals(r2[i])) return false;
                }
                return true;
            } else return false;
        }
        
    }
}
