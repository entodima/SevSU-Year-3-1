﻿using System;
using System.Collections.Generic;

namespace TPR_LR_4 {
    class Program {
        static void Main(string[] args) {
            float[,] a1 = {
                {1f,    4f,     3f,     1f,     3f,     4f},
                {0.25f, 1f,     7f,     3f,     0.2f,   1f},
                {0.333f,0.142f, 1f,     0.2f,   0.2f,   0.166f},
                {1f,    0.333f, 5f,     1f,     1f,     0.333f },
                {0.333f,5f,     5f,     1f,     1f,     3f},
                {0.25f, 1f,     6f,     3f,     0.333f, 1f}
            };

            List<float[,]> a2 = new List<float[,]> {
                new float[,] {
                    {1,     0.333f,     0.5f},
                    {3f,    1f,         3f},
                    {2f,    0.333f,     1f}
                },
                new float[,] {
                    {1,1,1},
                    {1,1,1},
                    {1,1,1}
                },
                new float[,] {
                    {1,5,1},
                    {0.2f,1,0.2f},
                    {1,5,1}
                },
                new float[,] {
                     {1,9,7},
                    {0.111f, 1, 0.2f},
                    {0.142f, 5, 1}
                },
                new float[,] {
                    {1,0.5f,1},
                    {2,1,2},
                    {1,0.5f,1}
                },
                new float[,] {
                    {1,6,4},
                    {0.166f,1,0.333f},
                    {0.25f,3,1}
                }
            };

            Console.WriteLine("A1:");
            var w1 = Calc(a1);

            var w2 = new List<float[]>();
            for (int i = 0; i < a2.Count; i++) {
                if (i % 2 == 0) Console.ForegroundColor = ConsoleColor.Blue;
                else Console.ForegroundColor = ConsoleColor.DarkRed;
                w2.Add(Calc(a2[i]));
                Console.ResetColor();
            }

            float[,] wO = new float[w2[w2.Count - 1].GetLength(0), w2.Count];
            for (int i = 0; i < wO.GetLength(0); i++)
                for (int j = 0; j < wO.GetLength(1); j++)
                    wO[i, j] = w2[j][i];
            Console.WriteLine("Вектор значений всех характеристик wO:");
            PrintMassMass(wO);

            float[] d = new float[wO.GetLength(0)];
            for (int i = 0; i < wO.GetLength(0); i++) {
                for (int j = 0; j < wO.GetLength(1); j++)
                    d[i] += wO[i, j] * w1[j];
            }
            Console.WriteLine("Оценка D каждого вида:");
            PrintMass(d);

            var best = CalcMax(d);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Лучшее решение №" + (best+1) + ": " + d[best]);
            Console.ResetColor();
        }

        static float[] Calc(float[,] a) {
            PrintMassMass(a);
            var wA1 = CreateW(a);
            Console.Write("W:\t");
            PrintMass(wA1);

            var w1A1 = CreateW1(a, wA1);
            var w2A1 = CreateW2(w1A1, wA1);

            var lMaxA1 = Lmax(w2A1);
            Console.WriteLine("Lmax:" + lMaxA1);

            var iS = CreateIS(lMaxA1, a.GetLength(0));
            Console.WriteLine("IS:" + iS);

            return wA1;
        }

        static void PrintMassMass(float[,] mass) {
            for(int i=0; i < mass.GetLength(0); i++) {
                for (int j = 0; j < mass.GetLength(1); j++) 
                    Console.Write(mass[i, j] + "\t"); 
                Console.WriteLine();
            }
        }
        static void PrintMass(float[] mass) {
            for (int j = 0; j < mass.GetLength(0); j++) 
                Console.Write(mass[j] + "\t");
            Console.WriteLine();
        }
        static void PrintList(List<float> list) {
            foreach(float element in list) {
                Console.Write(element + "\t");
            }
            Console.WriteLine();
        }
        static void PrintListList(List<List<float>> lists) {
            foreach(List<float> list in lists) {
                foreach (float element in list) {
                    Console.Write(element + "\t");
                }
                Console.WriteLine();
            }
        }

        static float[] CreateW(float[,] a) {
            //по 2 методу
            float[] returned = new float[a.GetLength(1)];
            var a_copy = (float[,])a.Clone();

            //сумма для каждого столбца
            float[] sumCols = new float[a.GetLength(0)];
            for(int i=0; i<a.GetLength(1); i++) {
                var temp = 0f;
                for(int j=0; j < a.GetLength(0); j++)
                    temp += a_copy[j, i];
                sumCols[i] = temp;
            }

                //делить элемент столбца на сумму столбца
            for (int i = 0; i < a.GetLength(1); i++) 
                for (int j = 0; j < a.GetLength(0); j++) 
                    a_copy[j, i] = a_copy[j, i] / sumCols[i];

                //сумма для каждой строки
            float[] sumStr = new float[a.GetLength(0)];
            for (int i = 0; i < a.GetLength(0); i++) {
                var temp = 0f;
                for (int j = 0; j < a.GetLength(1); j++)
                    temp += a_copy[i, j];
                sumStr[i] = temp;
            }

                //(сумма/количество) элемнетов в строке
            for(int i=0; i<sumStr.GetLength(0); i++) 
                returned[i] = sumStr[i] / a.GetLength(1);


            return returned;
        }

        static float[] CreateW1(float[,] a, float[] w) {
            float[] returned = new float[a.GetLength(1)];

            for(int i=0; i<a.GetLength(0); i++) {
                for (int j = 0; j < a.GetLength(1); j++)
                    returned[i] += a[i, j] * w[j];
            }

            return returned;
        }

        static float[] CreateW2(float[] w1, float[] w) {
            float[] returned = new float[w1.GetLength(0)];

            for (int i = 0; i < w1.GetLength(0); i++) 
                returned[i] = w1[i] / w[i];

            return returned;
        }

        static float Lmax(float[] w2) {
            var returned = 0f;
            for(int i=0; i<w2.GetLength(0); i++) {
                returned += w2[i] / w2.GetLength(0);
            }
            return returned;
        }

        static float CreateIS(float lMax, int n) {
            return (lMax-n)/(n-1);
        }

        static int CalcMax(float[] d) {
            int temp = 0;
            for(int i=0; i<d.GetLength(0); i++) {
                if (d[i] > d[temp]) temp = i;
            }
            return temp;
        }
    }
}   