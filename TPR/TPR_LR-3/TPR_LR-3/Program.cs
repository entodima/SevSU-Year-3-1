﻿using System;
using System.Collections.Generic;

namespace TPR_LR_3
{
    class Program
    {
        static void Main(string[] args) {
            int[,] a1 = {
                {0,0,0,2,0},
                {0,0,0,0,1},
                {0,0,0,3,0},
                {0,2,0,0,0},
                {0,0,0,0,0}
            };

            int[,] k = {
                {3,5,5,4,4},
                {4,4,4,5,4},
                {5,4,3,3,5},
                {3,5,3,5,3},
                {4,2,4,5,5},
                {3,5,3,5,3},
                {5,3,4,3,4},
                {4,5,3,4,3}
            };

            //создание массива N
            List<int> n1 = CreateN1List(a1);
            Console.Write("N-кратная модель: ");
            foreach (int nmb in n1) Console.Write(nmb + " ");
            Console.WriteLine();



            List<List<int>> K = new List<List<int>>();
            if (n1.Count != 0) {
                for (int i = 0; i < k.GetLength(0); i++) {
                    List<int> temp = new List<int>();
                    for (int j = 0; j < k.GetLength(1); j++) {
                        for (int l = 0; l < n1[j]; l++) {
                            temp.Add(k[i, j]);
                        }
                    }
                    K.Add(temp);
                }
                PrintListList(K);
                Console.WriteLine();

                foreach (List<int> row in K) {
                    row.Sort();
                    row.Reverse();
                }
                PrintListList(K);

                List<int> prime = new List<int>();
                for (int i = 0; i < k.GetLength(0); i++) {
                    prime.Add(i);
                }

                for (int i = 1; i < K.Count; i++) {
                    if (FirstListIsDominated(K[0], K[i])) {
                        prime = RemoveValueFromList(prime, i);
                    }
                }

                Console.Write("Вектор недоменируемых решений: ");
                PrintList(prime);

            } else {
                Console.WriteLine("N пусто");
            }

        }

        //создание n-кратной модели
        public static List<int> CreateN1List(int[,] a1) {
            List<int> NList = new List<int>();

            //создание порядка умножения
            var minRow = new List<int> { MinRow(a1) };

            List<List<int>> course = new List<List<int>>();
            var tempList = new List<int>();
            foreach(int element in minRow) tempList.Add(element);
            course.Add(tempList);

            for (int i=1; i<a1.GetLength(0); i++) {
                var tempList2 = new List<int>();
                foreach(int element in minRow) {
                    for (int j = 0; j < a1.GetLength(0); j++) {
                        if (a1[j, element] > 0) tempList2.Add(j);
                    }
                }
                course.Add(tempList2);
                minRow = tempList2;
            }

            Console.WriteLine("course:"); PrintListList(course);

            var multNlist = new List<int>();

            for(int i=0; i<course.Count-1; i++) {
                if (i == 0) multNlist.Add(1);
                else {
                    foreach (int element in course[i]) {
                        multNlist.Add(multNlist[i-1]*ValueInRow(a1, element));
                    }
                }
            }

            Console.WriteLine("multNlist:"); PrintList(multNlist);

            int[] tempMassNlist = new int[a1.GetLength(0)];

            for(int i=0; i<course.Count; i++) {
                if (i == 0) tempMassNlist[course[i][0]] = 1;
                else {
                    for (int j = 0; j < course[i].Count ; j++) {
                        tempMassNlist[course[i][j]] = multNlist[i + j];
                    }
                }
            }
            for (int i = 0; i < tempMassNlist.Length; i++) NList.Add(tempMassNlist[i]);

            return NList;
        }
        public static int MinRow(int[,] a1)
        {
            var returned = 0;
            for(int i=0; i< a1.GetLength(0); i++)
            {
                var temp = true;
                for(int j=0; j<a1.GetLength(1); j++)
                {
                    if (a1[i, j] > 0) temp = false;
                }
                if (temp) returned = i;
            }
            return returned;
        }
        public static int ValueInRow(int[,] a1, int row) {
            int returned = 0;
            for(int i=0; i<a1.GetLength(0); i++) {
                if (a1[row,i] != 0) return a1[row,i];
            }
            return returned;
        }

        static void PrintListList(List<List<int>> lists)
        {
            foreach(List<int> row in lists)
            {
                foreach(int col in row)
                {
                    Console.Write(col+" ");
                }
                Console.WriteLine();
            }
        }

        static void PrintList(List<int> list)
        {
            foreach(int element in list)
            {
                Console.Write(element +" ");
            }
            Console.WriteLine();
        }

        static List<int> RemoveValueFromList(List<int> list, int value)
        {
            for (int i = 0; i < list.Count; i++) if (value == list[i]) list.RemoveAt(i);
            return list;
        }

        public static bool FirstListIsDominated(List<int> list1, List<int> list2)
        {
            var returned = true;
            for(int i=0; i< list1.Count; i++)
            {
                if (list2[i] > list1[i])
                {
                    returned = false;
                    break;
                }
            }
            return returned;
        }
    }
}