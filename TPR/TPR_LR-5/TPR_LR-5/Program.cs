﻿using System;

namespace TPR_LR_5 {
    class Program {
        static void Main(string[] args) {
            //Исходная матрица
            int[,] matrix = {
                {7,7,7},
                {5,6,4},
                {5,3,3},
                {8,4,4},
                {6,2,6},
                {3,8,5},
                {6,4,3},
                {2,5,2},
                {9,2,5},
                {3,5,2}};
            int N = matrix.GetLength(0);
            int M = matrix.GetLength(1);

            int[] pX = new int[N]; //Парето-множество
            for(int i=0; i<N; i++) {
                pX[i] = 1;
            }
            int[] fMax = new int[M]; //Идеальная точка
            int Xi = 0; //Индекс эффективного решения
            float[] rX = new float[N]; //Расстояние до идеальной точки
            for(int i =0; i<N; i++) rX[i]= 0; 

            for (int l=0; l<N; l++) {       //не удоволетворяющие решения помечаются как 0,
                if(pX[l] == 1) {            //если решение недоминируемует 
                    for (int i=0;i<N;i++) { //(xj>xi не выполняется ни для одного xi ∈ X)
                        if (l != i) {       //и несравнимо с др. решениями
                            if (pX[i] == 1 && pX[l] == 1) {
                                if (
                                    (matrix[l,0] > matrix[i,0] &&
                                    matrix[l,1] >= matrix[i,1] && 
                                    matrix[l,2] >= matrix[i,2]) ||

                                    (matrix[l,0] >= matrix[i,0] &&
                                    matrix[l,1] > matrix[i,1] &&
                                    matrix[l,2] > matrix[i,2]) || 

                                    (matrix[l,0] > matrix[i,0] && 
                                    matrix[l,1] > matrix[i,1] &&
                                    matrix[l,2] > matrix[i,2]) ||

                                    (matrix[l,0] >= matrix[i,0] && 
                                    matrix[l,1] >= matrix[i,1] &&
                                    matrix[l,2] >= matrix[i,2])) {
                                    pX[i] = 0;
                                } else if (
                                    (matrix[l,0] < matrix[i,0] && 
                                    matrix[l,1] <= matrix[i,1] &&
                                    matrix[l,2] <= matrix[i,2]) ||

                                    (matrix[l,0] <= matrix[i,0] &&
                                    matrix[l,1] < matrix[i,1] &&
                                    matrix[l,2] < matrix[i,2]) ||

                                    (matrix[l,0] <= matrix[i,0] &&
                                    matrix[l,1] <= matrix[i,1] &&
                                    matrix[l,2] <= matrix[i,2]) ||

                                    (matrix[l,0] < matrix[i,0] &&
                                    matrix[l,1] < matrix[i,1] &&
                                    matrix[l,2] < matrix[i,2])) {
                                    pX[l] = 0;
                                }
                            }
                        }
                    }
                }
            }

            //вывод индексов элементов, "не вычеркнутых" из массива
            Console.Write("Парето-множество: ");
            for (int i =0; i<N; i++) {
                if (pX[i] == 1) {
                    Console.Write("x" + (i+1) + " ");
                }
            }
            Console.WriteLine("\n");
            
            //Поиск идеальной точки
            for(int i=0; i<M; i++) {
                fMax[i] = matrix[0,i];
            }
            for(int i = 0; i<M; i++) {
                for (int j=0; j<N; j++) {
                    if (matrix[j,i] > fMax[i]) {
                        fMax[i] = matrix[j,i];
                    }
                }
            }
            Console.WriteLine("Значения идеальной точки: (" + fMax[0] + "; " + fMax[1] + "; " + fMax[2] + ")" + "\n");
            Console.WriteLine("Расстояние от решения до идеальной точки: ");
            for (int i=0; i<N; i++) {
                if(pX[i] == 1) {
                    rX[i] = (float) Math.Sqrt(
                        Math.Pow(fMax[0] - matrix[i,0], 2) + 
                        Math.Pow(fMax[1] - matrix[i,1], 2) + 
                        Math.Pow(fMax[2] - matrix[i,2], 2));
                    Console.WriteLine("Rx" + (i+1) + " = " + rX[i] + ";");
                }
            }

            var temp = 0;
            for(int i=0; i<N; i++) {
                if (rX[i] < rX[temp] && rX[i]!=0) temp = i;
            }
            Xi = temp;

            Console.WriteLine("\nЭффективное решение: x" + (Xi+1));
        }
    }
}