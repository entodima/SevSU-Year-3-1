﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net_LR_3
{
    class Country
    {
        public string Name { get; set; }
        public string FormGov { get; set; }
        public int Square { get; set; }
        public string Regions { get; set; }

        public Country(string Name, string FormGov, int Square, string Regions)
        {
            this.Name = Name;
            this.FormGov = FormGov;
            this.Square = Square;
            this.Regions = Regions;
        }
    }
}
