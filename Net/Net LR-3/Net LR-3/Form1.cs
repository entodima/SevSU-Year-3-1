﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Net_LR_3
{
    public partial class Form1 : Form
    {
        private List<Country> countries = new List<Country>();

        public Form1()
        {
            InitializeComponent();

            listView1.View = View.Details;

            //задание заголовка таблицы
            listView1.Columns.Add("Название", 200, HorizontalAlignment.Left);
            listView1.Columns.Add("Форма правления", 200, HorizontalAlignment.Left);
            listView1.Columns.Add("Площадь км^2", 150, HorizontalAlignment.Left);
            listView1.Columns.Add("Список регионов", 300, HorizontalAlignment.Left);

            UpdateList();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();

            form2.ShowDialog();

            if (form2.Key == 1)
            {
                countries.Add(new Country(
                form2.GetName(),
                form2.GetForm(),
                Convert.ToInt32(form2.GetSquare()),
                form2.GetRegions()));
                UpdateList();
            }
        }


        public void UpdateList()
        {
            listView1.Items.Clear();
            if (countries.Count == 0)
            {
                listView1.Items.Add(new ListViewItem(new string[]{
                    "Пусто", "*", "*",  "*"}));
            }
            else foreach (Country country in countries)
                {
                    listView1.Items.Add(new ListViewItem(new string[]{
                    country.Name,
                    country.FormGov,
                    country.Square.ToString(),
                    country.Regions
                }));
                }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3();
            form3.ShowDialog();

            if (form3.Key == 1)
            {
                for (int i = 0; i < countries.Count; i++)
                {
                    
                    if (countries[i].Name == form3.GetName())
                    {
                        Console.WriteLine(countries[i].Name);
                        countries.RemoveAt(i);
                    }
                }
                UpdateList();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.ShowDialog();

            if (form2.Key == 3)
            {
                for (int i = 0; i < countries.Count; i++)
                {
                    if (countries[i].Name == form2.GetName())
                    {
                        countries[i].FormGov = form2.GetForm();
                        countries[i].Square = Int32.Parse(form2.GetSquare());
                        countries[i].Regions = form2.GetRegions();
                    }
                }
                UpdateList();
            }
        }
    }
}
