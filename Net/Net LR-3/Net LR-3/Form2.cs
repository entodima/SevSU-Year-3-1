﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Net_LR_3
{
    public partial class Form2 : Form
    {
        public int Key;
        public Form2()
        {
            InitializeComponent();
            textBox3.Text = "0";
        }
        

        private void Form2_Load(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Key = 1;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Key = 2;
            this.Close();
        }

        public string GetName()
        {
            return textBox1.Text;
        }

        public string GetForm()
        {
            return textBox2.Text;
        }

        public string GetSquare()
        {
            return textBox3.Text;
        }

        public string GetRegions()
        {
            return textBox4.Text;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Key = 3;
            this.Close();
        }
    }
}
