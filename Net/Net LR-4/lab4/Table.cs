using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace lab4
{
    public partial class Table : Form
    {
        public BindingList<Country> Countries = new BindingList<Country>();
        public DataGridView DataGridView { get { return dataGridView1;} }
        public Table()
        {
            InitializeComponent();
            dataGridView1.DataSource = Countries;
            dataGridView1.AutoSizeColumnsMode=DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView1.AutoSizeRowsMode=DataGridViewAutoSizeRowsMode.AllCells;
            
            Closing += (sender,e) =>
            {
                Container c = (Container) MdiParent;
                c.ChildrenOpen--;
            };
        }



        public Table(IEnumerable<Country> museumsCollection): this()
        {
            foreach (Country tiger in museumsCollection)
            {
                Countries.Add(tiger);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}