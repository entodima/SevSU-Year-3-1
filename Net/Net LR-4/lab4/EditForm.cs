using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace lab4
{
    public partial class EditForm : Form
    {
        public Country Result;
        public string ImageName;

        public EditForm()
        {
            InitializeComponent();
        }

        public EditForm(Country t)
        {
            InitializeComponent();

            nameField.Text = t.Name;
            ownerField.Text = t.FormGov;
            addressField.Text = t.Regions;
            visitsLabel.Text = t.Square.ToString();
            visitsField.Value = t.Square;
            ImageName = t.FileName;
            pictureBox1.Image = Image.FromFile(ImageName);
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
        
            visitsLabel.Text = $"{visitsField.Value}";
        
    }
        
        private void button1_Click(object sender, EventArgs e)
        {
            string name = nameField.Text ?? "����� ������";
            string owner = ownerField.Text ?? "������";
            string address = addressField.Text ?? "��";
            int visits = visitsField.Value;
            
            Result = new Country(name, owner, address, visits, ImageName);
            

            DialogResult = DialogResult.OK;
            Close();
        }

        private void EditForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                button1_Click(sender, e);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                ImageName = ofd.FileName;
                try
                {
                    pictureBox1.Image = Image.FromFile(ImageName);
                    pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                }
                catch (System.OutOfMemoryException) { return; }
            }

        }
    }
    }
