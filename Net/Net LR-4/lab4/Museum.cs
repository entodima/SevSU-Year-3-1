using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab4
{
    [Serializable]
    public class Country
    {
        public string Name { get; set; }
        public string FormGov { get; set; }
        public string Regions { get; set; }
        public int Square { get; set; }
        public string FileName { get; set; }
        

        public Country(string name, string form, string regions, int square, string filename)
        {
            Name = name;
            FormGov = form;
            Regions = regions;
            Square = square;
            FileName = filename;
        }
    }
}