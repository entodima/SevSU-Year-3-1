﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;

namespace Net_LR_1 {
    class Program {
        static void Main(string[] args) {

            //Задание 1
            Console.Write("Введите размерность матрицы: ");
            int size = Int32.Parse(Console.ReadLine());
            int[,] arr = new int[size, size];

            Random rnd = new Random();
            //заполенение массива случайными значениями
            for (int i = 0; i < size; i++) for (int j = 0; j < size; j++)
                    arr[i, j] = rnd.Next(1, 10);
            PrintArray(arr);

            List<List<int>> values = new List<List<int>>();

            for(int i = 0; i < arr.GetLength(0); i++) {
                int minInRow = 11;
                int lastI = 0;
                int lastJ = 0;
                for (int j = 0; j < arr.GetLength(0); j++) {
                    if (minInRow > arr[i, j]) {
                        minInRow = arr[i, j];
                        lastI = i;
                        lastJ = j;
                    }
                }
                values.Add(new List<int> { minInRow, lastI,lastJ});
                minInRow = 11;
            }

            PrintList(values);

            List<int> value = new List<int> { 0, 0, 0};
            for (int i = 0; i < values.Count; i++) {
                for (int j = 0; j < 1; j++) {
                    if (values[i][j] > value[0]) value = values[i];
                }
            }
            PrintList(value);


            //Задание 2
            Console.Write("Введите строку: ");
            string str = Console.ReadLine();

            Console.Write("Введите размер слова: ");
            int n = Int32.Parse(Console.ReadLine());

            string[] strArr = str.Split(' ');
            strArr = strArr.OrderBy(x => x).ToArray();

            PrintArray(strArr, n);



            //Задание 3
            Console.Write("Введите строку: ");
            str = Console.ReadLine();
            strArr = str.Split(' ');
            List<int> nmbList = new List<int>();

            foreach(string element in strArr) {
                int nmb;
                if(int.TryParse(element, out nmb)) {
                    if (element.Length == 7) nmbList.Add(nmb);
                }
            }

            PrintList(nmbList);

            Console.ReadKey();
        }

        static void PrintArray(string[] arr, int n) {
            foreach (string element in arr) if (element.Length==n) Console.Write(element + "\n");
            Console.WriteLine();
            Console.WriteLine("------------------------------");
        }

        static void PrintArray(int[,] arr) {
            for (int i = 0; i < arr.GetLength(0); i++) {
                for (int j = 0; j < arr.GetLength(0); j++) Console.Write(arr[i, j] + "\t");
                Console.WriteLine();
            }
            Console.WriteLine("------------------------------");
        }

        static void PrintList(List<List<int>> lists) {
            foreach (List<int> list in lists) {
                foreach (int element in list) Console.Write(element + "   ");
                Console.WriteLine();
            }
            Console.WriteLine("------------------------------");
        }
        static void PrintList(List<int> list) {
            foreach (int element in list) Console.Write(element + "   ");
            Console.WriteLine();
            Console.WriteLine("------------------------------");
        }
        static void PrintList(List<string> list) {
            foreach (string element in list) Console.Write(element + "   ");
            Console.WriteLine();
            Console.WriteLine("------------------------------");
        }
    }
}
